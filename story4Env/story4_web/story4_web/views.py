from django.http import HttpResponse
from django.shortcuts import render

def about(request):
    return(render(request,'about.html'))

def work(request):
    return(render(request,'work.html'))

def contact(request):
    return(render(request,'contact.html'))

def soon(request):
    return(render(request,'media.html'))